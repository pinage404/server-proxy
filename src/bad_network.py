import itertools
import random
import typing
import flask
import more_itertools

from .response_to_iterable import Response
from .response_to_iterable import response_to_iterable
from .to_callable import CallableOfFloatOrFloat
from .to_callable import to_callable


AbandonRate: typing.TypeAlias = CallableOfFloatOrFloat


def bad_network(
    abandon_rate: AbandonRate = 1 / 100,
    chunk_size: int = 2**10,
):
    abandon_rate = to_callable(abandon_rate)

    should_continue_streaming = lambda _: random.random() > abandon_rate()

    def decorated(func: typing.Callable[[typing.Any], Response]):
        def wrapper(*args, **kwargs):
            @flask.stream_with_context
            def response_with_delay_between_chunks():
                response = func(*args, **kwargs)

                response_iterator = response_to_iterable(response)

                response_chunked = map(
                    bytes, more_itertools.batched(response_iterator, chunk_size)
                )

                response_maybe_cutted = itertools.takewhile(
                    should_continue_streaming, response_chunked
                )

                for chunks in response_maybe_cutted:
                    yield chunks

            return response_with_delay_between_chunks()

        wrapper.__name__ = func.__name__

        return wrapper

    return decorated
