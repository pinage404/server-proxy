from flask import (
    Flask,
    Response,
    jsonify,
    make_response,
    request,
)
from flask_cors import CORS
from os import getenv
from pprint import pprint
from random import uniform as rand

from .bad_network import bad_network
from .log import log
from .cache import cache
from .delay import (
    LATENCY,
    delay,
)
from .slow_network import (
    DOWNLOAD_SPEED,
    KO,
    MO,
    Mbit,
    slow_network,
)
from .pass_to_server import pass_to_server


app = Flask(__name__)
CORS(app)


REAL_SERVER_URL = getenv("REAL_SERVER_URL")

MAIN_HTTP_METHODS = [
    "HEAD",
    "GET",
    "POST",
    "PUT",
    "DELETE",
    "OPTIONS",
]


@app.route("/", methods=MAIN_HTTP_METHODS)
@app.route("/<path:path>", methods=MAIN_HTTP_METHODS)
# @delay()
# @delay(seconds_to_wait=2.0)
# @delay(seconds_to_wait=LATENCY["2G"])
# @delay(seconds_to_wait=lambda: rand(0.0, 3.0))
# @slow_network()
# @slow_network(download_octets_by_second=1 * MO)
# @slow_network(download_octets_by_second=DOWNLOAD_SPEED["2G"])
# @slow_network(download_octets_by_second=lambda: rand(200 * KO, 1 * MO))
# @bad_network()
# @bad_network(abandon_rate=1 / 1000)
# @bad_network(abandon_rate=lambda: rand(1 / 1000, 1 / 100))
# @cache(REAL_SERVER_URL)
# @log(REAL_SERVER_URL)
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
