import typing

CallableOfFloatOrFloat: typing.TypeAlias = typing.Callable[[], float] | float


def to_callable(maybe_callable: CallableOfFloatOrFloat) -> typing.Callable[[], float]:
    if callable(maybe_callable):
        return maybe_callable

    return lambda: maybe_callable
