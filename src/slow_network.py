import itertools
from random import uniform as rand
import time
import typing
import flask
import more_itertools

from .response_to_iterable import response_to_iterable
from .response_to_iterable import Response
from .to_callable import CallableOfFloatOrFloat
from .to_callable import to_callable


KO = 2**10
MO = KO * KO
Mbit = MO / 8

OctectsBySecond: typing.TypeAlias = CallableOfFloatOrFloat


# https://kenstechtips.com/index.php/download-speeds-2g-3g-and-4g-actual-meaning#2G_3G_4G_5G_Download_Speeds
DOWNLOAD_SPEED: typing.Dict[str, OctectsBySecond] = {
    "2G": lambda: rand(0.05 * Mbit, 0.1 * Mbit),
    "EDGE": lambda: rand(0.1 * Mbit, 0.3 * Mbit),
    "3G": lambda: rand(1.5 * Mbit, 7.2 * Mbit),
    "4G": lambda: rand(15 * Mbit, 150 * Mbit),
    "5G": lambda: rand(150 * Mbit, 200 * Mbit),
}


def slow_network(
    download_octets_by_second: OctectsBySecond = DOWNLOAD_SPEED["3G"],
    chunks_by_second: float = 100,
):
    download_octets_by_second = to_callable(download_octets_by_second)

    def decorated(func: typing.Callable[[typing.Any], Response]):
        def wrapper(*args, **kwargs):
            @flask.stream_with_context
            def response_with_delay_between_chunks():
                response = func(*args, **kwargs)

                response_iterator = response_to_iterable(response)

                chunk_size = max(
                    int(download_octets_by_second() // chunks_by_second),
                    1,
                )

                response_chunked = map(
                    bytes, more_itertools.batched(response_iterator, chunk_size)
                )

                delay_between_packet_in_second = 1 / chunks_by_second

                for chunks in response_chunked:
                    yield chunks
                    time.sleep(delay_between_packet_in_second)

            return response_with_delay_between_chunks()

        wrapper.__name__ = func.__name__

        return wrapper

    return decorated
