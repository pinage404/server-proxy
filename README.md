# Server Proxy

Small utility that helps to tweak HTTP server using a [Flask](https://flask.palletsprojects.com) server

## Schemas

### Normal usage

```mermaid
flowchart LR
    client["Client"] --> server["Real server"]
```

### Usage with proxy

```mermaid
flowchart LR
    proxy["`
        **Proxy server**
        You can tweak it
        This is hot reload
    `"]
    client["Client"] --> proxy --> server["Real server"]
```

## Usage

1. Install [Docker](https://docs.docker.com/get-docker/)
1. Install [Docker Compose](https://docs.docker.com/compose/install/)
1. Clone [this repository](https://gitlab.com/pinage404/server_proxy)

   ```shell
   git clone https://gitlab.com/pinage404/server_proxy
   ```

1. Update [`REAL_SERVER_URL` in `docker-compose.yml`](https://gitlab.com/pinage404/server_proxy/-/blob/main/docker-compose.yml#L11)
1. Change the URL of your API in your client to `http://localhost:5000` (by default)
1. Start server proxy

   ```shell
   docker-compose up
   ```

## Features

Whatever you will do by adding a route using the [Flask API](https://flask.palletsprojects.com/en/1.1.x/api/#url-route-registrations) or tweaking the current code

### Pass request to the real server

This act same as without this proxy

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@pass_to_server(REAL_SERVER_URL) # <-- the decorator who does it
def proxy_to_server():
    pass
```

### Log full request and response

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@log(REAL_SERVER_URL) # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

### In memory cache response

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@cache(REAL_SERVER_URL) # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

### Delay response

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@delay() # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

#### Random delay

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@delay(seconds_to_wait=lambda: rand(0.0, 3.0)) # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

#### Simulate mobile network delay

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@delay(seconds_to_wait=LATENCY["2G"]) # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

### Simulate slow network

The response is split into chunks and the chunks are sent at intervals

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@slow_network() # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

#### Random slowness

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@slow_network(download_octets_by_second=lambda: rand(200 * KO, 1 * MO)) # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

#### Simulate mobile network slowness

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@slow_network(download_octets_by_second=DOWNLOAD_SPEED["2G"]) # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

### Simulate bad network

The response is split into chunks and the chunks are sent, but there is a probability that the chunks will stop being sent

As if the connection had been lost during transfer

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@bad_network() # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

#### Adjust randomness

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@bad_network(abandon_rate=lambda: rand(1 / 1000, 1 / 100)) # <-- the decorator who does it
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

### Fake response

Just normal Flask usage

```python
@app.route('/api/user', methods=['GET'])
def user():
    return jsonify({
        'name': 'foo',
        'id': 404,
    })
```

### Multi servers support

Add [`service`](https://docs.docker.com/compose/compose-file/05-services/) in the [`compose.yml`](https://gitlab.com/pinage404/server_proxy/-/blob/main/compose.yml)

Exemple:

```yaml
services:
    user_api:
        build: .
        volumes:
            - $PWD:/srv/server_proxy
        ports:
            - "5000:5000"
        env_file:
            - .env
        network_mode: host # Linux
        extra_hosts:
            host.docker.internal: host-gateway
        tty: true
        command: sh run.sh

    document_api:
        build: .
        volumes:
            - $PWD:/srv/server_proxy
        ports:
            - "6000:6000"
        env_file:
            - .env
        environment:
            PORT: 6000
        network_mode: host # Linux
        extra_hosts:
            host.docker.internal: host-gateway
        tty: true
        command: sh run.sh
```

In your client configuration change the API's URL

```diff
-user-api: https://example.com/api/user/v2/
+user-api: http://localhost:5000
-document-api: https://example.com/api/document/v1/
+document-api: http://localhost:6000
```

---

If you need to do advanced stuff with Flask, take a look at [Awesome Flask](https://github.com/mjhea0/awesome-flask)

## License

[ISC License](./LICENSE)
