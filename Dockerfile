FROM python:3.12-alpine

RUN adduser --disabled-password python
USER python

ENV PATH /home/python/.local/bin:$PATH

WORKDIR /srv/server_proxy

RUN pip install poetry
